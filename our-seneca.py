from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from tinydb import TinyDB, Query
import time
app = FastAPI()

db = TinyDB('esp-cpu-tmp.json')
temperature_table = db.table('temperature')

class TemperatureData(BaseModel):
    time: float
    temp: float

@app.post("/seneca/")
def store_temperature(temperature_data: TemperatureData):
    temperature_dict = temperature_data.dict()
    temperature_dict["time"] = time.time()
    temperature_id = temperature_table.insert(temperature_dict)
    return {"id": temperature_id, **temperature_dict}

@app.get("/seneca/")
def get_temperatures():
    return temperature_table.all()

@app.get("/seneca/{temperature_id}")
def get_temperature(temperature_id: int):
    temperature = temperature_table.get(doc_id=temperature_id)
    if temperature:
        return temperature
    else:
        raise HTTPException(status_code=404, detail="Temperature data not found")
