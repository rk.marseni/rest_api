from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from typing import List
from tinydb import TinyDB, Query

app = FastAPI()

# Initialize TinyDB database
db = TinyDB('db.json')
orders_table = db.table('orders')

# Model representing an order item
class OrderItem(BaseModel):
    item_id: int
    quantity: int

# Model representing an order with multiple items
class Order(BaseModel):
    customer_name: str
    items: List[OrderItem]

# POST method to create an order
@app.post("/orders/")
def create_order(order: Order):
    order_dict = order.dict()
    order_id = orders_table.insert(order_dict)
    return {"order_id": order_id, **order_dict}

# GET method to retrieve all orders
@app.get("/orders/")
def get_orders():
    return orders_table.all()

# GET method to retrieve a specific order by ID
@app.get("/orders/{order_id}")
def get_order(order_id: int):
    order = orders_table.get(doc_id=order_id)
    if order:
        return order
    else:
        raise HTTPException(status_code=404, detail="Order not found")
