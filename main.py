from fastapi import FastAPI

app = FastAPI()

students = {1:{'name': 'Prayush', 'age': 20},
            2:{'name': 'Prjwol', 'age': 21},
            3:{'name': 'Pratik', 'age': 22},
            4:{'name': 'Aarya', 'age': 16}}
@app.get("/")
def read_root():
    return {"Hello": "Sujan Gurung"}

@app.get("/students")
def get_students():
    return students